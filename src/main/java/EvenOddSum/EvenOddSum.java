package EvenOddSum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EvenOddSum {

    public static List<Integer> evenOdd (OneArgIntOperator operator, List<Integer> intList) {
        List<Integer> result = new ArrayList<Integer>();
        for (Integer item : intList) {
            if(operator.op(item)) {
                result.add(item);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<Integer> randomList = new ArrayList<Integer>();
        randomList = Arrays.asList(1, 25, 46, 33, 2, 5, 58, 236, 777, 693, 8, 0, 55, 12, 214);

        OneArgIntOperator even = x -> x % 2 == 0;
        OneArgIntOperator odd = x -> x % 2 == 1;

        List<Integer> mustBeEven = evenOdd(even, randomList);
        System.out.println("Парні числа: " + mustBeEven);
        System.out.println("Сума парних чисел: " + mustBeEven.stream().mapToInt(e -> e).sum());

        List<Integer> mustBeOdd = evenOdd(odd, randomList);
        System.out.println("Непарні числа: " + mustBeOdd);
        System.out.println("Сума непарних чисел: " + mustBeOdd.stream().mapToInt(e -> e).sum());
    }

}

package EvenOddSum;

public interface OneArgIntOperator {
     boolean op(int a);
}

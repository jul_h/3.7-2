package SquareNumbers;

public class Main {
    public static void main(String[] args) {
        SquareNumbers squareNumbers = new SquareNumbers();

        squareNumbers.makeRandomList();

        squareNumbers.square();

        squareNumbers.squareIntSum();
    }
}

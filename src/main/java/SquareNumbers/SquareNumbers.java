package SquareNumbers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class SquareNumbers {
    private int minVal;
    private int maxVal;
    private List<Integer> randomNumbers;

    public SquareNumbers() {
        minVal = 1;
        maxVal = 100;
        randomNumbers = new ArrayList<>();
    }

    public void makeRandomList() {
        for (int i = 0; i < 10; i++) {
            int randInt = ThreadLocalRandom.current().nextInt(minVal, maxVal);
            randomNumbers.add(randInt);
        }
        System.out.println("Список із 10 рандомних чисел: " + randomNumbers);
    }

    public void square() {
        List<Integer> squareNumbers = randomNumbers.stream().map(x -> x * x).collect(Collectors.toList());
        System.out.println("Список із квадратів 10 рандомних чисел: " + squareNumbers);
    }

    public void squareIntSum() {
        int squareSum = randomNumbers.stream().map(x -> x * x).reduce(0, Integer::sum);
        System.out.println("Сума квадратів 10 рандомних чисел: " + squareSum);
    }
}

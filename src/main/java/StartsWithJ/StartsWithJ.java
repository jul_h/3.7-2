package StartsWithJ;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StartsWithJ {
    private List<String> colleaguesSurnames;
    private static final Faker faker = new Faker();

    public StartsWithJ(int colleaguesQuantity) {
        colleaguesSurnames = new ArrayList<>();

        for (int i = 0; i < colleaguesQuantity; i++) {
            colleaguesSurnames.add(lastName());
        }
    }

    private String lastName() {
        return faker.name().lastName();
    }

    public void lastNamesWithFirstJ() {
        Stream<String> lastNamesStartWithJ = colleaguesSurnames.stream().filter(s -> s.startsWith("J"));
        System.out.println("Прізвища, що починаються на J: " + lastNamesStartWithJ.collect(Collectors.toList()));
    }

    @Override
    public String toString() {
        String result = "";
        for (String colleaguesSurname : colleaguesSurnames) {
            result = result + colleaguesSurname + "\n";
        }
        return result;
    }
}

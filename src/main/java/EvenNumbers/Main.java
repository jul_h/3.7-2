package EvenNumbers;

public class Main {
    public static void main(String[] args) {
        EvenNumbers evenNumbers = new EvenNumbers();

        evenNumbers.makeRandomList();

        evenNumbers.filterEvenNumbers();
    }
}

package EvenNumbers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class EvenNumbers {

        private int minValue;
        private int maxValue;
        private List<Integer> randNumb;

        public EvenNumbers() {
            minValue = 1;
            maxValue = 2500;
            randNumb = new ArrayList<>();
        }

        public void makeRandomList() {
            for (int i = 0; i < 20; i++) {
                int randInteger = ThreadLocalRandom.current().nextInt(minValue, maxValue);
                randNumb.add(randInteger);
            }
            System.out.println("Список із 20 рандомних чисел: " + randNumb);
        }

        public void filterEvenNumbers() {
            List<Integer> evenInt = randNumb.stream().filter(x -> x % 2 == 0).collect(Collectors.toList());
            System.out.println("Список парних чисел: " + evenInt);
        }
}
